original=~/.cache/wal/colors.sh
temp=~/.cache/wal/colors_temp.sh

cp $original $temp

sed -i "s/^color/export\ color/g" $temp

. $temp

printf "colors:
  primary:
    background: '$background'
    foreground: '$foreground'
  normal:
   black:   '$color0'
   red:     '$color1'
   green:   '$color2'
   yellow:  '$color3'
   blue:    '$color4'
   magenta: '$color5'
   cyan:    '$color6'
   white:   '$color7'
  bright:
   black:   '$color8'
   red:     '$color9'
   green:   '$color10'
   yellow:  '$color11'
   blue:    '$color12'
   magenta: '$color13'
   cyan:    '$color14'
   white:   '$color15'
" > ~/.config/alacritty/colors.yml
