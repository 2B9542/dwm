set shell := ["fish", "-c"]

default:
	@just --list --unsorted

install:
    sudo rm config.h && sudo make install

fix-pywal:
  sed -i '17d;' ~/.cache/wal/colors-wal-dwm.h

alacritty-colors:
  bash -c './ac.sh'
